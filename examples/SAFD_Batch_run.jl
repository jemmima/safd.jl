
using SAFD
using BenchmarkTools
using Dates

using MS_Import

##############################################################################
# This is an example of how to process the data in batch mode.

##############################################################################
# Import parameters
path="/path/to/the/file"
#filenames=["filename.mzXML"]
mz_thresh=[0,380]

# Feature detection parameters
max_numb_iter=10
max_t_peak_w=300 # or 30
res=20000
min_ms_w=0.02
r_thresh=0.75
min_int=2000
sig_inc_thresh=5
S2N=2

min_peak_w_s=3


GC.gc()

function feature_detect_batch(path,max_numb_iter,
    max_t_peak_w,res,min_ms_w,r_thresh,min_int,sig_inc_thresh,S2N,min_peak_w_s,mz_thresh)


    mz_vales_mat,mz_int_mat,t0_mat,t_end_mat,m_mat,pathin_mat,Rt_mat=import_files_batch_MS1(path,mz_thresh)

    Max_it=repeat([max_numb_iter],size(pathin_mat,1))
    Max_t_peak_w=repeat([max_t_peak_w],size(pathin_mat,1))
    Res=repeat([res],size(pathin_mat,1))
    Min_ms_w=repeat([min_ms_w],size(pathin_mat,1))
    R_tresh=repeat([r_tresh],size(pathin_mat,1))
    Min_int=repeat([min_int],size(pathin_mat,1))
    Sig_inc_thresh=repeat([sig_inc_thresh],size(pathin_mat,1))
    S2N1=repeat([S2N],size(pathin_mat,1))
    Min_peak_w_s=repeat([min_peak_w_s],size(pathin_mat,1))



    for i=1:size(pathin_mat,1)
        #safd(mz_vales_mat[i],
        #mz_int_mat[i],t0_mat[i],t_end_mat[i],m_mat[i],pathin_mat[i],Max_it[i],
        #Max_t_peak_w[i],Res[i],Min_ms_w[i],R_tresh[i],Min_int[i],Sig_inc_thresh[i],
        #S2N1[i],Min_peak_w_s[i])

        safd_s3D(mz_vales_mat[i],
        mz_int_mat[i],Rt_mat[i],m_mat[i],pathin_mat[i],Max_it[i],
        Max_t_peak_w[i],Res[i],Min_ms_w[i],R_tresh[i],Min_int[i],Sig_inc_thresh[i],
        S2N1[i],Min_peak_w_s[i])

    end

    #write a output file for the used processing variables
    mzt1 = mz_thresh[1]
    mzt2 = mz_thresh[2]
    open(joinpath(path,"SAFD settings.txt"), "w") do io
        write(io,  "Processing date = "*string(Dates.today())*"\nProcessing path = "*path*"\nLoading vars\n\t mz_thresh = $mzt1 - $mzt2 \n"*
                   "Feature detection vars\n\tmax_numb_iter = $max_numb_iter\n\tres = $res\n\tr_thresh = $r_thresh\n\tmax_t_peak_w = $max_t_peak_w\n\tmin_ms_w = $min_ms_w\n\tmin_int = $min_int\n\tsig_inc_thresh = $sig_inc_thresh\n\tS2N = $S2N\n\tmin_peak_w_s = $min_peak_w_s\n")
    end

end




@time feature_detect_batch(pathin,max_numb_iter,
    max_t_peak_w,res,min_ms_w,r_tresh,min_int,sig_inc_thresh,S2N,min_peak_w_s,mz_thresh)
